import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Класс осуществляет поиск максимальной цифры
 *
 * @author Лукьянов В. А.
 */

public class Main {
    private static final long MAX_DIGIT_LIMIT = (long) Math.pow(10, 9);

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                long digit = sc.nextLong();
                System.out.println("Ответ: " + search(digit));
            } catch (InputMismatchException e) {
                throw new InputMismatchException("Число должно быть целочисленным");
            }
        }
    }

    /**
     * Метод осуществляет поиск максимальной цифры
     *
     * @param input_digit - цифра
     * @return - максимальную цифру
     */
    static long search(long input_digit) {
        long max_digit = 0;
        try {
            if (checkLimit(input_digit) && checkNegativeDigit(input_digit)) {
                while (input_digit > max_digit) {
                    long digit = input_digit % 10;
                    if (max_digit < digit) {
                        max_digit = digit;
                    }
                    input_digit /= 10;
                }
            } else {
                throw new ArithmeticException("Число введено не корректно");
            }
        } catch (ArithmeticException e) {
            e.printStackTrace();
        }
        return max_digit;
    }

    /**
     * Метод проверяет на цифру на задданый лимит
     *
     * @param digit - цифра
     * @return - true или false
     */
    static boolean checkLimit(long digit) {
        boolean permission = true;
        if (digit > MAX_DIGIT_LIMIT) {
            permission = false;
        }
        return permission;
    }

    /**
     * Метод проверяет на отрицательные цифры
     *
     * @param digit - цифра
     * @return - true или false
     */
    static boolean checkNegativeDigit(long digit) {
        boolean permission = true;
        if (digit < 0) {
            permission = false;
        }
        return permission;
    }
}
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    /**
     * Осуществляет тестирование метода search на цифру 0
     */
    @Test
    void zero() {
        assertEquals(0, Main.search(0));
    }

    /**
     * Осуществляет тестирование метода search на положительную цифру
     */
    @Test
    void positiveDigit() { ;
        assertEquals(6, Main.search(456443));
    }

    /**
     * Осуществляет тестирование метода checkLimit на проверку лимита разрешённой цифры
     */
    @Test
    void maxLimit() { ;
        assertFalse(Main.checkLimit(1000000001));
    }

    /**
     * Осуществляет тестирование метода checkNegativeDigit на отрицательную цифру
     */
    @Test
    void negtiveDigit() { ;
        assertFalse(Main.checkNegativeDigit(-4));
    }
}